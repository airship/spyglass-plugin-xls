# Copyright 2019 AT&T Intellectual Property.  All other rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from spyglass.exceptions import SpyglassBaseException


class NoSpecMatched(SpyglassBaseException):
    message = 'No spec matched. The available specs are: %(excel_specs)'


class ExcelFileNotSpecified(SpyglassBaseException):
    message = 'Engineering excel file not specified'


class ExcelSpecNotSpecified(SpyglassBaseException):
    message = 'Engineering excel spec not specified'


class InvalidSpec(SpyglassBaseException):
    message = (
        'Series type dataset is missing iter index type. '
        'Possible index types are "row" or "col".')


class MissingData(SpyglassBaseException):
    message = 'No %(missing_data) specified for %(section)'


class ExcelSheetNotFound(SpyglassBaseException):
    message = (
        'Sheet name %(sheet_name) could not be resolved in the given '
        'Excel files.')
