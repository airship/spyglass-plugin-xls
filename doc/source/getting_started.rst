..
      Copyright 2019 AT&T Intellectual Property.
      All Rights Reserved.

      Licensed under the Apache License, Version 2.0 (the "License"); you may
      not use this file except in compliance with the License. You may obtain
      a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
      WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
      License for the specific language governing permissions and limitations
      under the License.

===============
Getting Started
===============

This document explains the steps to installing and setting up the Spyglass
Excel plugin.

1.) Clone the Spyglass Excel plugin::

    git clone https://opendev.org/airship/spyglass-plugin-xls.git

2.) Clone Spyglass::

    git clone https://opendev.org/airship/spyglass.git

3.) Install required packages in Spyglass directory::

    pip3 install pipenv && pipenv install

4.) Launch the pipenv from your spyglass directory::

    pipenv shell

5.) Install Spyglass and the Spyglass Excel plugin into the pipenv::

    pip3 install . && pip3 install ../spyglass-plugin-xls/.

6.) You can now run Spyglass commands from the pipenv.

Examples
--------

1. Required Input (Refer to 'spyglass_plugin_xls/examples' folder to get these
inputs)

    a) Excel File: SiteDesignSpec_v0.1.xlsx
    b) Excel Spec: excel_spec_upstream.yaml
    c) Site Config: 'spyglass/examples/site_config.yaml'
    d) Template_dir: 'spyglass/examples/templates'
    e) Site name: airship-seaworthy

2. Spyglass CLI Command using an Excel File:

.. code-block:: bash

    spyglass excel documents -i -x ../spyglass-plugin-xls/spyglass_plugin_xls/examples/SiteDesignSpec_v0.1.xlsx \
           -e ../spyglass-plugin-xls/spyglass_plugin_xls/examples/excel_spec.yaml \
           -c ../spyglass-plugin-xls/spyglass_plugin_xls/examples/site_config.yaml \
           -s airship-seaworthy -t spyglass/examples/templates/

3. Spyglass CLI Command using a CSV File:

.. code-block:: bash

    spyglass excel documents -i -x ../spyglass-plugin-xls/spyglass_plugin_xls/examples/Site-Information.csv \
           -e ../spyglass-plugin-xls/spyglass_plugin_xls/examples/excel_spec.yaml \
           -c ../spyglass-plugin-xls/spyglass_plugin_xls/examples/site_config.yaml \
           -s airship-seaworthy -t spyglass/examples/templates/
