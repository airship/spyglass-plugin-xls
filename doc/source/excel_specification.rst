..
      Copyright 2019 AT&T Intellectual Property.
      All Rights Reserved.

      Licensed under the Apache License, Version 2.0 (the "License"); you may
      not use this file except in compliance with the License. You may obtain
      a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
      WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
      License for the specific language governing permissions and limitations
      under the License.

===================
Excel Specification
===================

Excel Spec is like an index to the Excel sheet to look for the data to be
collected by the tool. The excel_spec.yaml file provides a sample structure for
an excel specification based off the SiteDesignSpec_v0.1.xlsx sheet. (Both can
be found in the examples folder)

Below is the definition for each key in the Excel spec

+ **ipmi**

  * type - How the data is stored (series/container/point)
  * sheet_name - Name of the sheet from where IPMI information is to be read
    (File name, excluding the file extension, when using a CSV file)
  * iter

    + index-type - How data is indexed (row/col)
    + start - Row/Col number where ipmi data begins
    + end - Row/Col number where ipmi data ends

  * data

    + hostname - location of the hostname (row/col/[r, c])
    + ipmi_address - location of the ipmi address (row/col/[r, c])
    + ipmi_gateway - location of the ipmi gateway (row/col/[r, c])
    + host_profile - location of the host profile (row/col/[r, c])

+ **private_vlan**

  * type - How the data is stored (series/container/point)
  * sheet_name - Name of the sheet from where private vlan information is to
    be read (File name, excluding the file extension, when using a CSV file)
  * no_sanitize - Names of values that should not be sanitized
  * iter

    + index-type - How data is indexed (row/col)
    + start - Row/Col number where private vlan data begins
    + end - Row/Col number where private vlan data ends

  * data

    + net_type - location of the net type (row/col/[r, c])
    + vlan - location of the vlan (row/col/[r, c])

+ **private_net**

  * type - How the data is stored (series/container/point)
  * sheet_name - Name of the sheet from where private net information is to
    be read (File name, excluding the file extension, when using a CSV file)
  * iter

    + index-type - How data is indexed (row/col)
    + start - Row/Col where private vlan data begins
    + end - Row/Col where private vlan data ends

  * data

    + vlan - location of the vlan (row/col/[r, c])
    + ip - location of the ip (row/col/[r, c])

+ **public**

  * type - How the data is stored (series/container/point)
  * sheet_name - Name of the sheet from where public information is to be
    read (File name, excluding the file extension, when using a CSV file)
  * data

    + oam

      * type - How the data is stored (series/container/point)
      * data

        + vlan - location of the vlan (row/col/[r, c])
        + ip - location of the ip (row/col/[r, c])

    + ingress

      * type - How the data is stored (series/container/point)
      * data

        + ip - location of the ip (row/col/[r, c])

    + oob

      * type - How the data is stored (series/container/point)
      * iter

        + index-type - How data is indexed (row/col)
        + start - Row/Col where private vlan data begins
        + end - Row/Col where private vlan data ends

      * data

        + ip - location of the ip (row/col/[r, c])

+ **site_info**

  * type - How the data is stored (series/container/point)
  * sheet_name - Name of the sheet from where site information is to be read
    (File name, excluding the file extension, when using a CSV file)
  * sanitize - If data should be sanitised (boolean)
  * data

    + domain - location of the domain (row/col/[r, c])
    + subdomain - location of the subdomain (row/col/[r, c])
    + global_group - location of the global group (row/col/[r, c])
    + ldap - location of the ldap (row/col/[r, c])
    + ntp - location of the ntp (row/col/[r, c])
    + dns - location of the dns (row/col/[r, c])

+ **location**

  * type - How the data is stored (series/container/point)
  * sheet_name - Name of the sheet from where location information is to be
    read (File name, excluding the file extension, when using a CSV file)
  * sanitize - If data should be sanitised (boolean)
  * data

    + sitename - location of the sitename (row/col/[r, c])
    + corridor - location of the corridor (row/col/[r, c])
    + state - location of the state (row/col/[r, c])
    + country - location of the country (row/col/[r, c])
    + clli - location of the clli (row/col/[r, c])
