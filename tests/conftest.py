# Copyright 2019 AT&T Intellectual Property.  All other rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pytest


@pytest.fixture(scope='class')
def raw_excel_data(request):
    request.cls.raw_excel_data = {
        'ipmi': [
            {
                'hostname': 'cab2r72c12',
                'ipmi_address': '10.0.220.138',
                'ipmi_gateway': '10.0.220.129',
                'host_profile': 'dp-r720'
            }, {
                'hostname': 'cab2r72c13',
                'ipmi_address': '10.0.220.139',
                'ipmi_gateway': '10.0.220.129',
                'host_profile': 'dp-r720'
            }, {
                'hostname': 'cab2r72c14',
                'ipmi_address': '10.0.220.140',
                'ipmi_gateway': '10.0.220.129',
                'host_profile': 'dp-r720'
            }, {
                'hostname': 'cab2r72c15',
                'ipmi_address': '10.0.220.141',
                'ipmi_gateway': '10.0.220.129',
                'host_profile': 'dp-r720'
            }, {
                'hostname': 'cab2r72c16',
                'ipmi_address': '10.0.220.142',
                'ipmi_gateway': '10.0.220.129',
                'host_profile': 'cp-r720'
            }, {
                'hostname': 'cab2r72c17',
                'ipmi_address': '10.0.220.143',
                'ipmi_gateway': '10.0.220.129',
                'host_profile': 'cp-r720'
            }, {
                'hostname': 'cab2r73c12',
                'ipmi_address': '10.0.220.170',
                'ipmi_gateway': '10.0.220.161',
                'host_profile': 'dp-r720'
            }, {
                'hostname': 'cab2r73c13',
                'ipmi_address': '10.0.220.171',
                'ipmi_gateway': '10.0.220.161',
                'host_profile': 'dp-r720'
            }, {
                'hostname': 'cab2r73c14',
                'ipmi_address': '10.0.220.172',
                'ipmi_gateway': '10.0.220.161',
                'host_profile': 'dp-r720'
            }, {
                'hostname': 'cab2r73c15',
                'ipmi_address': '10.0.220.173',
                'ipmi_gateway': '10.0.220.161',
                'host_profile': 'dp-r720'
            }, {
                'hostname': 'cab2r73c16',
                'ipmi_address': '10.0.220.174',
                'ipmi_gateway': '10.0.220.161',
                'host_profile': 'cp-r720'
            }, {
                'hostname': 'cab2r73c17',
                'ipmi_address': '10.0.220.175',
                'ipmi_gateway': '10.0.220.161',
                'host_profile': 'cp-r720'
            }
        ],
        'private_vlan': [
            {
                'net_type': 'iSCSI/Storage',
                'vlan': 'vlan23'
            }, {
                'net_type': 'PXE',
                'vlan': 'vlan21'
            }, {
                'net_type': 'Calico BGP peering addresses',
                'vlan': 'vlan22'
            }, {
                'net_type': 'Overlay',
                'vlan': 'vlan24'
            }, {
                'net_type': 'CNI Pod addresses',
                'vlan': 'n/a'
            }
        ],
        'private_net': [
            {
                'vlan': 'vlan23',
                'ip': '30.31.1.0/25'
            }, {
                'vlan': 'vlan21',
                'ip': '30.30.4.0/25'
            }, {
                'vlan': 'vlan21',
                'ip': '30.30.4.128/25'
            }, {
                'vlan': 'vlan21',
                'ip': '30.30.5.0/25'
            }, {
                'vlan': 'vlan21',
                'ip': '30.30.5.128/25'
            }, {
                'vlan': 'vlan22',
                'ip': '30.29.1.0/25'
            }, {
                'vlan': 'vlan24',
                'ip': '30.19.0.0/25'
            }
        ],
        'public': {
            'oam': {
                'vlan': 'vlan-21',
                'ip': '10.0.220.0/26'
            },
            'ingress': {
                'ip': '10.0.220.72/29'
            },
            'oob': [
                {
                    'ip': '10.0.220.128/27'
                }, {
                    'ip': '10.0.220.160/27'
                }, {
                    'ip': '10.0.220.192/27'
                }, {
                    'ip': '10.0.220.224/27'
                }
            ]
        },
        'site_info': {
            'domain': 'dmy00.example.com',
            'subdomain': 'testitservices',
            'global_group': 'AA-AAA-dmy00',
            'ldap': 'url: ldap://ldap.example.com',
            'ntp': '150.234.210.5 (ns1.example.com)',
            'dns': '40.40.40.40 (ntp1.example.com),'
            '\n41.41.41.41 (ntp2.example.com)'
        },
        'location': {
            'sitename': 'SampleSiteName',
            'corridor': 'Corridor 1',
            'state': 'New Jersey',
            'country': 'SampleCountry',
            'clli': 'XXXXXX21'
        }
    }


@pytest.fixture(scope='class')
def site_data(request):
    request.cls.site_data = {
        'ipmi_data': [
            {
                'cab2r72c12': {
                    'ipmi_address': '10.0.220.138',
                    'ipmi_gateway': '10.0.220.129',
                    'host_profile': 'dp-r720'
                },
                'cab2r72c13': {
                    'ipmi_address': '10.0.220.139',
                    'ipmi_gateway': '10.0.220.129',
                    'host_profile': 'dp-r720'
                },
                'cab2r72c14': {
                    'ipmi_address': '10.0.220.140',
                    'ipmi_gateway': '10.0.220.129',
                    'host_profile': 'dp-r720'
                },
                'cab2r72c15': {
                    'ipmi_address': '10.0.220.141',
                    'ipmi_gateway': '10.0.220.129',
                    'host_profile': 'dp-r720'
                },
                'cab2r72c16': {
                    'ipmi_address': '10.0.220.142',
                    'ipmi_gateway': '10.0.220.129',
                    'host_profile': 'cp-r720'
                },
                'cab2r72c17': {
                    'ipmi_address': '10.0.220.143',
                    'ipmi_gateway': '10.0.220.129',
                    'host_profile': 'cp-r720'
                },
                'cab2r73c12': {
                    'ipmi_address': '10.0.220.170',
                    'ipmi_gateway': '10.0.220.161',
                    'host_profile': 'dp-r720'
                },
                'cab2r73c13': {
                    'ipmi_address': '10.0.220.171',
                    'ipmi_gateway': '10.0.220.161',
                    'host_profile': 'dp-r720'
                },
                'cab2r73c14': {
                    'ipmi_address': '10.0.220.172',
                    'ipmi_gateway': '10.0.220.161',
                    'host_profile': 'dp-r720'
                },
                'cab2r73c15': {
                    'ipmi_address': '10.0.220.173',
                    'ipmi_gateway': '10.0.220.161',
                    'host_profile': 'dp-r720'
                },
                'cab2r73c16': {
                    'ipmi_address': '10.0.220.174',
                    'ipmi_gateway': '10.0.220.161',
                    'host_profile': 'cp-r720'
                },
                'cab2r73c17': {
                    'ipmi_address': '10.0.220.175',
                    'ipmi_gateway': '10.0.220.161',
                    'host_profile': 'cp-r720'
                },
            },
            [
                'cab2r72c12',
                'cab2r72c13',
                'cab2r72c14',
                'cab2r72c15',
                'cab2r72c16',
                'cab2r72c17',
                'cab2r73c12',
                'cab2r73c13',
                'cab2r73c14',
                'cab2r73c15',
                'cab2r73c16',
                'cab2r73c17',
            ]
        ],
        'network_data': {
            'private': {
                'iSCSI/Storage': {
                    'vlan': 'vlan23',
                    'subnet': ['30.31.1.0/25'],
                    'is_common': True
                },
                'PXE': {
                    'vlan': 'vlan21',
                    'subnet': [
                        '30.30.4.0/25', '30.30.4.128/25', '30.30.5.0/25',
                        '30.30.5.128/25'
                    ],
                    'is_common': True
                },
                'Calico BGP peering addresses': {
                    'vlan': 'vlan22',
                    'subnet': ['30.29.1.0/25'],
                    'is_common': True
                },
                'Overlay': {
                    'vlan': 'vlan24',
                    'subnet': ['30.19.0.0/25'],
                    'is_common': True
                }
            },
            'public': {
                'oam': {
                    'subnet': ['10.0.220.0/26'],
                    'vlan': 'vlan21'
                },
                'ingress': '10.0.220.72/29',
                'oob': {
                    'subnet': [
                        '10.0.220.128/27', '10.0.220.160/27',
                        '10.0.220.192/27', '10.0.220.224/27'
                    ]
                }
            }
        },
        'site_info': {
            'location': {
                'corridor': 'Corridor 1',
                'name': 'SampleSiteName',
                'state': 'New Jersey',
                'country': 'SampleCountry',
                'physical_location': 'XXXXXX21'
            },
            'dns': [
                '40.40.40.40', '(ntp1.example.com)', '41.41.41.41',
                '(ntp2.example.com)'
            ],
            'ntp': ['150.234.210.5', '(ns1.example.com)'],
            'domain': 'dmy00.example.com',
            'ldap': {
                'subdomain': 'testitservices',
                'common_name': 'AA-AAA-dmy00',
                'url': 'url: ldap://ldap.example.com'
            }
        }
    }
