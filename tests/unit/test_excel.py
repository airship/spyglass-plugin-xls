# Copyright 2019 AT&T Intellectual Property.  All other rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from copy import deepcopy
import os
import unittest
from unittest import mock

import pytest
from spyglass.data_extractor import models

from spyglass_plugin_xls.excel import ExcelPlugin
from spyglass_plugin_xls.excel_parser import ExcelParser
from spyglass_plugin_xls.exceptions import ExcelFileNotSpecified
from spyglass_plugin_xls.exceptions import ExcelSpecNotSpecified

FIXTURE_DIR = os.path.join(
    os.path.dirname(os.path.dirname(__file__)), 'shared')

EXCEL_SPEC_PATH = os.path.join(FIXTURE_DIR, 'excel_spec.yaml')

INVALID_EXCEL_SPEC_PATH = os.path.join(FIXTURE_DIR, 'invalid_excel_spec.yaml')

EXCEL_FILE_PATH = os.path.join(FIXTURE_DIR, 'SiteDesignSpec_v0.1.xlsx')

SITE_CONFIG_PATH = os.path.join(FIXTURE_DIR, 'site_config.yaml')


@pytest.mark.usefixtures('site_data')
class TestExcelPlugin(unittest.TestCase):
    """Tests for ExcelPlugin"""
    def test___init__(self):
        region = 'test_region'
        result = ExcelPlugin(
            region, excel_file=EXCEL_FILE_PATH, excel_spec=EXCEL_SPEC_PATH)
        self.assertEqual(region, result.region)
        self.assertEqual('excel', result.SOURCE_TYPE)
        self.assertEqual('spyglass-plugin-xls', result.PLUGIN_NAME)
        self.assertEqual(EXCEL_FILE_PATH, result.excel_path)
        self.assertEqual(EXCEL_SPEC_PATH, result.excel_spec)
        self.assertTrue(result.raw_data)

    def test___init___no_excel_file(self):
        region = 'test_region'
        with self.assertRaises(ExcelFileNotSpecified):
            ExcelPlugin(region, excel_spec=EXCEL_SPEC_PATH)

    def test___init___no_excel_spec(self):
        region = 'test_region'
        with self.assertRaises(ExcelSpecNotSpecified):
            ExcelPlugin(region, excel_file=EXCEL_FILE_PATH)

    def test_load_raw_data(self):
        region = 'test_region'
        obj = ExcelPlugin(
            region, excel_file=EXCEL_FILE_PATH, excel_spec=EXCEL_SPEC_PATH)
        with mock.patch.object(ExcelParser, 'get_data') as mock_get_data:
            mock_get_data.return_value = 'success'
            obj.load_raw_data()
        mock_get_data.assert_called_once()
        self.assertEqual('success', obj.raw_data)

    def test_parse_racks(self):
        region = 'test_region'
        obj = ExcelPlugin(
            region, excel_file=EXCEL_FILE_PATH, excel_spec=EXCEL_SPEC_PATH)
        obj.parsed_xl_data = deepcopy(self.site_data)
        result = obj.parse_racks()
        self.assertEqual(2, len(result))
        for rack in result:
            self.assertIsInstance(rack, models.Rack)
            self.assertEqual(6, len(rack.hosts))
            for host in rack.hosts:
                self.assertIn(host.name, self.site_data['ipmi_data'][0])
                self.assertEqual(
                    self.site_data['ipmi_data'][0][host.name]['host_profile'],
                    host.host_profile)

    def test_parse_hosts(self):
        region = 'test_region'
        obj = ExcelPlugin(
            region, excel_file=EXCEL_FILE_PATH, excel_spec=EXCEL_SPEC_PATH)
        obj.parsed_xl_data = deepcopy(self.site_data)
        result = obj.parse_hosts()
        self.assertEqual(12, len(result))
        for host in result:
            self.assertIn(host.name, self.site_data['ipmi_data'][0])
            self.assertEqual(
                self.site_data['ipmi_data'][0][host.name]['host_profile'],
                host.host_profile)

    def test_parse_hosts_using_rack(self):
        region = 'test_region'
        obj = ExcelPlugin(
            region, excel_file=EXCEL_FILE_PATH, excel_spec=EXCEL_SPEC_PATH)
        obj.parsed_xl_data = deepcopy(self.site_data)
        result = obj.parse_hosts('rack73')
        self.assertEqual(6, len(result))
        for host in result:
            self.assertIn(host.name, self.site_data['ipmi_data'][0])
            self.assertEqual('rack73', host.rack_name)
            self.assertNotIn('r72', host.name)
            self.assertEqual(
                self.site_data['ipmi_data'][0][host.name]['host_profile'],
                host.host_profile)

    def test_parse_networks(self):
        expected_network_types = {
            'oob': {
                'type': 'public',
                'name': 'oob'
            },
            'oam': {
                'type': 'public',
                'name': 'oam'
            },
            'calico': {
                'type': 'private',
                'name': 'Calico BGP peering addresses'
            },
            'ingress': {
                'type': 'public',
                'name': 'ingress'
            },
            'overlay': {
                'type': 'private',
                'name': 'Overlay'
            },
            'pxe': {
                'type': 'private',
                'name': 'PXE'
            },
            'storage': {
                'type': 'private',
                'name': 'iSCSI/Storage'
            }
        }
        network_data = deepcopy(self.site_data['network_data'])
        region = 'test_region'
        obj = ExcelPlugin(
            region, excel_file=EXCEL_FILE_PATH, excel_spec=EXCEL_SPEC_PATH)
        obj.parsed_xl_data = deepcopy(self.site_data)
        result = obj.parse_networks()
        self.assertEqual(7, len(result))
        for vlan_data in result:
            self.assertIn(vlan_data.name, expected_network_types)
            data = expected_network_types[vlan_data.name]
            if vlan_data.name != 'ingress':
                self.assertEqual(
                    network_data[data['type']][data['name']]['subnet'],
                    vlan_data.subnet)
            else:
                self.assertEqual(
                    network_data[data['type']][data['name']],
                    vlan_data.subnet[0])

    def test_parse_ips(self):
        region = 'test_region'
        obj = ExcelPlugin(
            region, excel_file=EXCEL_FILE_PATH, excel_spec=EXCEL_SPEC_PATH)
        obj.parsed_xl_data = deepcopy(self.site_data)
        host_name = 'cab2r72c15'
        result = obj.parse_ips(host_name)
        self.assertIsInstance(result, models.IPList)
        self.assertEqual(
            self.site_data['ipmi_data'][0][host_name]['ipmi_address'],
            result.oob)

    def test_parse_ldap_information(self):
        expected_ldap_data = deepcopy(self.site_data['site_info']['ldap'])
        expected_ldap_data['domain'] = 'example'
        expected_ldap_data['url'] = expected_ldap_data['url'].split(' ')[1]
        region = 'test_region'
        obj = ExcelPlugin(
            region, excel_file=EXCEL_FILE_PATH, excel_spec=EXCEL_SPEC_PATH)
        obj.parsed_xl_data = deepcopy(self.site_data)
        result = obj.parse_ldap_information()
        self.assertDictEqual(expected_ldap_data, result)

    def test_parse_ntp_servers(self):
        expected_ntp_servers = deepcopy(self.site_data['site_info']['ntp'][:1])
        region = 'test_region'
        obj = ExcelPlugin(
            region, excel_file=EXCEL_FILE_PATH, excel_spec=EXCEL_SPEC_PATH)
        obj.parsed_xl_data = deepcopy(self.site_data)
        result = obj.parse_ntp_servers()
        self.assertIsInstance(result, models.ServerList)
        self.assertEqual(expected_ntp_servers, result.servers)

    def test_parse_dns_servers(self):
        expected_dns_servers = [
            self.site_data['site_info']['dns'][0],
            self.site_data['site_info']['dns'][2]
        ]
        region = 'test_region'
        obj = ExcelPlugin(
            region, excel_file=EXCEL_FILE_PATH, excel_spec=EXCEL_SPEC_PATH)
        obj.parsed_xl_data = deepcopy(self.site_data)
        result = obj.parse_dns_servers()
        self.assertIsInstance(result, models.ServerList)
        self.assertEqual(expected_dns_servers, result.servers)

    def test_parse_domain_name(self):
        region = 'test_region'
        obj = ExcelPlugin(
            region, excel_file=EXCEL_FILE_PATH, excel_spec=EXCEL_SPEC_PATH)
        obj.parsed_xl_data = deepcopy(self.site_data)
        result = obj.parse_domain_name()
        self.assertEqual(self.site_data['site_info']['domain'], result)

    def test_parse_location_information(self):
        expected_location_data = deepcopy(
            self.site_data['site_info']['location'])
        expected_location_data['corridor'] = 'c1'
        expected_location_data[
            'physical_location_id'] = expected_location_data.pop(
                'physical_location')
        region = 'test_region'
        obj = ExcelPlugin(
            region, excel_file=EXCEL_FILE_PATH, excel_spec=EXCEL_SPEC_PATH)
        obj.parsed_xl_data = deepcopy(self.site_data)
        result = obj.parse_location_information()
        self.assertDictEqual(expected_location_data, result)

    def test__get_network_name_from_vlan_name(self):
        result = ExcelPlugin._get_network_name_from_vlan_name('ksn')
        self.assertEqual('calico', result)
        result = ExcelPlugin._get_network_name_from_vlan_name('calico')
        self.assertEqual('calico', result)
        result = ExcelPlugin._get_network_name_from_vlan_name('storage')
        self.assertEqual('storage', result)
        result = ExcelPlugin._get_network_name_from_vlan_name('oam')
        self.assertEqual('oam', result)
        result = ExcelPlugin._get_network_name_from_vlan_name('server')
        self.assertEqual('oam', result)
        result = ExcelPlugin._get_network_name_from_vlan_name('ovs')
        self.assertEqual('overlay', result)
        result = ExcelPlugin._get_network_name_from_vlan_name('overlay')
        self.assertEqual('overlay', result)
        result = ExcelPlugin._get_network_name_from_vlan_name('oob')
        self.assertEqual('oob', result)
        result = ExcelPlugin._get_network_name_from_vlan_name('pxe')
        self.assertEqual('pxe', result)

    def test__get_network_name_from_vlan_name_dne(self):
        result = ExcelPlugin._get_network_name_from_vlan_name('dne')
        self.assertEqual('', result)

    def test__get_formatted_server_list(self):
        test_list = [
            '124.1.23.54', '(example.com)', '192.168.1.0',
            '(anotherexample.com)'
        ]
        expected_list = ['124.1.23.54', '192.168.1.0']
        result = ExcelPlugin._get_formatted_server_list(test_list)
        self.assertIsInstance(result, models.ServerList)
        self.assertEqual(expected_list, result.servers)

    def test__get_rack(self):
        region = 'test_region'
        obj = ExcelPlugin(
            region, excel_file=EXCEL_FILE_PATH, excel_spec=EXCEL_SPEC_PATH)
        result = obj._get_rack('cab2r72c15')
        self.assertEqual('r72', result)

    def test__get_rackwise_hosts(self):
        expected_data = {
            'rack72': [
                'cab2r72c12', 'cab2r72c13', 'cab2r72c14', 'cab2r72c15',
                'cab2r72c16', 'cab2r72c17'
            ],
            'rack73': [
                'cab2r73c12', 'cab2r73c13', 'cab2r73c14', 'cab2r73c15',
                'cab2r73c16', 'cab2r73c17'
            ]
        }
        region = 'test_region'
        obj = ExcelPlugin(
            region, excel_file=EXCEL_FILE_PATH, excel_spec=EXCEL_SPEC_PATH)
        obj.parsed_xl_data = deepcopy(self.site_data)
        result = obj._get_rackwise_hosts()
        self.assertDictEqual(expected_data, result)

    def test__get_rack_data(self):
        expected_data = {'r72': 'rack72', 'r73': 'rack73'}
        region = 'test_region'
        obj = ExcelPlugin(
            region, excel_file=EXCEL_FILE_PATH, excel_spec=EXCEL_SPEC_PATH)
        obj.parsed_xl_data = deepcopy(self.site_data)
        result = obj._get_rack_data()
        self.assertDictEqual(expected_data, result)
