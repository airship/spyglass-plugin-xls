# Copyright 2019 AT&T Intellectual Property.  All other rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import unittest

from openpyxl import Workbook
from openpyxl.worksheet.worksheet import Worksheet
import pytest
import yaml

from spyglass_plugin_xls.excel_parser import ExcelParser
from spyglass_plugin_xls.exceptions import ExcelSheetNotFound

FIXTURE_DIR = os.path.join(
    os.path.dirname(os.path.dirname(__file__)), 'shared')

EXCEL_SPEC_PATH = os.path.join(FIXTURE_DIR, 'excel_spec.yaml')

INVALID_EXCEL_SPEC_PATH = os.path.join(FIXTURE_DIR, 'invalid_excel_spec.yaml')

EXCEL_FILE_PATH = os.path.join(FIXTURE_DIR, 'SiteDesignSpec_v0.1.xlsx')

CSV_FILE_PATH = os.path.join(FIXTURE_DIR, "Site-Information.csv")

SITE_CONFIG_PATH = os.path.join(FIXTURE_DIR, 'site_config.yaml')


@pytest.mark.usefixtures('raw_excel_data')
@pytest.mark.usefixtures('site_data')
class TestExcelParser(unittest.TestCase):
    """Tests for ExcelParser"""

    SPEC = 'xl_spec'
    maxDiff = None

    def test___init__(self):
        with open(EXCEL_SPEC_PATH, 'r') as f:
            loaded_spec = yaml.safe_load(f)
        result = ExcelParser(EXCEL_FILE_PATH, EXCEL_SPEC_PATH, self.SPEC)
        self.assertEqual(EXCEL_FILE_PATH, result.file_name)
        self.assertDictEqual(loaded_spec, result.excel_specs)
        self.assertDictEqual(
            loaded_spec['specs'][self.SPEC], result.loaded_spec)
        self.assertDictEqual(self.raw_excel_data, result.loaded_data)
        self.assertIsInstance(result.wb_combined, Workbook)
        self.assertEqual('xl_spec', result.spec)

    def test___init__csv(self):
        with open(EXCEL_SPEC_PATH, 'r') as f:
            loaded_spec = yaml.safe_load(f)
        result = ExcelParser(CSV_FILE_PATH, EXCEL_SPEC_PATH, self.SPEC)
        self.assertEqual(CSV_FILE_PATH, result.file_name)
        self.assertDictEqual(loaded_spec, result.excel_specs)
        self.assertDictEqual(
            loaded_spec['specs'][self.SPEC], result.loaded_spec)
        self.assertDictEqual(self.raw_excel_data, result.loaded_data)
        self.assertIsInstance(result.wb_combined, Workbook)
        self.assertEqual('xl_spec', result.spec)

    def test_sanitize(self):
        test_string = 'Hello THIS is A TeSt'
        expected_output = 'hellothisisatest'
        result = ExcelParser.sanitize(test_string)
        self.assertEqual(expected_output, result)

    def test_compare(self):
        test_string1 = 'These strings are equal.'
        test_string2 = 'These strIngs are Equal .'
        obj = ExcelParser(EXCEL_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.compare(test_string1, test_string2)
        self.assertTrue(result)

    def test_compare_csv(self):
        test_string1 = 'These strings are equal.'
        test_string2 = 'These strIngs are Equal .'
        obj = ExcelParser(CSV_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.compare(test_string1, test_string2)
        self.assertTrue(result)

    def test_compare_false(self):
        test_string1 = 'These strings are not equal.'
        test_string2 = 'These strIngs are Equal.'
        obj = ExcelParser(EXCEL_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.compare(test_string1, test_string2)
        self.assertFalse(result)

    def test_compare_false_csv(self):
        test_string1 = 'These strings are not equal.'
        test_string2 = 'These strIngs are Equal.'
        obj = ExcelParser(CSV_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.compare(test_string1, test_string2)
        self.assertFalse(result)

    def test__get_workbook(self):
        obj = ExcelParser(EXCEL_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj._get_workbook('Site-Information')
        self.assertIsInstance(result, Worksheet)

    def test__get_workbook_csv(self):
        obj = ExcelParser(CSV_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj._get_workbook('Site-Information')
        self.assertIsInstance(result, Worksheet)

    def test__check_sanitize_settings(self):
        test_data_sanitize_only = {'sanitize': False}
        sanitize, no_sanitize_keys = ExcelParser._check_sanitize_settings(
            test_data_sanitize_only)
        self.assertFalse(sanitize)
        self.assertFalse(no_sanitize_keys)
        test_data_no_sanitize_keys = {
            'no_sanitize': ['here', 'is', 'some', 'keys']
        }
        sanitize, no_sanitize_keys = ExcelParser._check_sanitize_settings(
            test_data_no_sanitize_keys)
        self.assertTrue(sanitize)
        self.assertEqual(
            test_data_no_sanitize_keys['no_sanitize'], no_sanitize_keys)

    def test_extract_data_points(self):
        expected_data = self.raw_excel_data['public']['oam']
        obj = ExcelParser(EXCEL_FILE_PATH, EXCEL_SPEC_PATH, self.SPEC)
        result = obj.extract_data_points(
            obj.loaded_spec['public']['data']['oam'], 'Site-Information')
        self.assertDictEqual(expected_data, result)

    def test_extract_data_points_csv(self):
        expected_data = self.raw_excel_data['public']['oam']
        obj = ExcelParser(CSV_FILE_PATH, EXCEL_SPEC_PATH, self.SPEC)
        result = obj.extract_data_points(
            obj.loaded_spec['public']['data']['oam'], 'Site-Information')
        self.assertDictEqual(expected_data, result)

    def test_extract_data_points_unsanitized(self):
        expected_data = self.raw_excel_data['location']
        obj = ExcelParser(EXCEL_FILE_PATH, EXCEL_SPEC_PATH, self.SPEC)
        result = obj.extract_data_points(obj.loaded_spec['location'])
        self.assertDictEqual(expected_data, result)

    def test_extract_data_points_unsanitized_csv(self):
        expected_data = self.raw_excel_data['location']
        obj = ExcelParser(CSV_FILE_PATH, EXCEL_SPEC_PATH, self.SPEC)
        result = obj.extract_data_points(obj.loaded_spec['location'])
        self.assertDictEqual(expected_data, result)

    def test_extract_data_series(self):
        expected_data = self.raw_excel_data['ipmi']
        obj = ExcelParser(EXCEL_FILE_PATH, EXCEL_SPEC_PATH, self.SPEC)
        result = obj.extract_data_series(obj.loaded_spec['ipmi'])
        self.assertEqual(expected_data, result)

    def test_extract_data_series_csv(self):
        expected_data = self.raw_excel_data['ipmi']
        obj = ExcelParser(CSV_FILE_PATH, EXCEL_SPEC_PATH, self.SPEC)
        result = obj.extract_data_series(obj.loaded_spec['ipmi'])
        self.assertEqual(expected_data, result)

    def test_extract_data_series_no_sanitize(self):
        expected_data = self.raw_excel_data['private_vlan']
        obj = ExcelParser(EXCEL_FILE_PATH, EXCEL_SPEC_PATH, self.SPEC)
        result = obj.extract_data_series(obj.loaded_spec['private_vlan'])
        self.assertEqual(expected_data, result)

    def test_extract_data_series_no_sanitize_csv(self):
        expected_data = self.raw_excel_data['private_vlan']
        obj = ExcelParser(CSV_FILE_PATH, EXCEL_SPEC_PATH, self.SPEC)
        result = obj.extract_data_series(obj.loaded_spec['private_vlan'])
        self.assertEqual(expected_data, result)

    def test_extract_data_using_spec(self):
        expected_data = self.raw_excel_data
        obj = ExcelParser(EXCEL_FILE_PATH, EXCEL_SPEC_PATH, self.SPEC)
        result = obj.extract_data_using_spec(obj.loaded_spec)
        self.assertDictEqual(expected_data, result)

    def test_extract_data_using_spec_csv(self):
        expected_data = self.raw_excel_data
        obj = ExcelParser(CSV_FILE_PATH, EXCEL_SPEC_PATH, self.SPEC)
        result = obj.extract_data_using_spec(obj.loaded_spec)
        self.assertDictEqual(expected_data, result)

    def test_get_ipmi_data(self):
        expected_hosts = self.site_data['ipmi_data'][1]
        expected_ipmi_data = self.site_data['ipmi_data'][0]
        obj = ExcelParser(EXCEL_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.get_ipmi_data()
        self.assertDictEqual(result[0], expected_ipmi_data)
        self.assertEqual(result[1], expected_hosts)

    def test_get_ipmi_data_csv(self):
        expected_hosts = self.site_data['ipmi_data'][1]
        expected_ipmi_data = self.site_data['ipmi_data'][0]
        obj = ExcelParser(CSV_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.get_ipmi_data()
        self.assertDictEqual(result[0], expected_ipmi_data)
        self.assertEqual(result[1], expected_hosts)

    def test_get_private_vlan_data(self):
        expected_vlan_data = {
            'vlan23': 'iSCSI/Storage',
            'vlan21': 'PXE',
            'vlan22': 'Calico BGP peering addresses',
            'vlan24': 'Overlay',
            'na': 'CNI Pod addresses'
        }
        obj = ExcelParser(EXCEL_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.get_private_vlan_data()
        self.assertDictEqual(expected_vlan_data, result)

    def test_get_private_vlan_data_csv(self):
        expected_vlan_data = {
            'vlan23': 'iSCSI/Storage',
            'vlan21': 'PXE',
            'vlan22': 'Calico BGP peering addresses',
            'vlan24': 'Overlay',
            'na': 'CNI Pod addresses'
        }
        obj = ExcelParser(CSV_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.get_private_vlan_data()
        self.assertDictEqual(expected_vlan_data, result)

    def test_get_private_network_data(self):
        expected_network_data = self.site_data['network_data']['private']
        obj = ExcelParser(EXCEL_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.get_private_network_data()
        self.assertDictEqual(expected_network_data, result)

    def test_get_private_network_data_csv(self):
        expected_network_data = self.site_data['network_data']['private']
        obj = ExcelParser(CSV_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.get_private_network_data()
        self.assertDictEqual(expected_network_data, result)

    def test_get_public_network_data(self):
        expected_network_data = self.site_data['network_data']['public']
        obj = ExcelParser(EXCEL_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.get_public_network_data()
        self.assertEqual(expected_network_data, result)

    def test_get_public_network_data_csv(self):
        expected_network_data = self.site_data['network_data']['public']
        obj = ExcelParser(CSV_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.get_public_network_data()
        self.assertEqual(expected_network_data, result)

    def test_get_site_info(self):
        expected_site_info = self.site_data['site_info']
        obj = ExcelParser(EXCEL_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.get_site_info()
        self.assertDictEqual(expected_site_info, result)

    def test_get_site_info_csv(self):
        expected_site_info = self.site_data['site_info']
        obj = ExcelParser(CSV_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.get_site_info()
        self.assertDictEqual(expected_site_info, result)

    def test_get_location_data(self):
        expected_location_data = self.site_data['site_info']['location']
        obj = ExcelParser(EXCEL_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.get_location_data()
        self.assertEqual(expected_location_data, result)

    def test_get_location_data_csv(self):
        expected_location_data = self.site_data['site_info']['location']
        obj = ExcelParser(CSV_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.get_location_data()
        self.assertEqual(expected_location_data, result)

    def test_validate_sheet_names_with_spec(self):
        obj = ExcelParser(EXCEL_FILE_PATH, EXCEL_SPEC_PATH)
        self.assertIsNone(obj.validate_sheet_names_with_spec())

    def test_validate_sheet_names_with_spec_csv(self):
        obj = ExcelParser(CSV_FILE_PATH, EXCEL_SPEC_PATH)
        self.assertIsNone(obj.validate_sheet_names_with_spec())

    def test_validate_sheet_names_with_spec_invalid(self):
        with self.assertRaises(ExcelSheetNotFound):
            ExcelParser(EXCEL_FILE_PATH, INVALID_EXCEL_SPEC_PATH)

    def test_validate_sheet_names_with_spec_invalid_csv(self):
        with self.assertRaises(ExcelSheetNotFound):
            ExcelParser(CSV_FILE_PATH, INVALID_EXCEL_SPEC_PATH)

    def test_get_data(self):
        expected_data = self.site_data
        obj = ExcelParser(EXCEL_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.get_data()
        self.assertDictEqual(expected_data, result)

    def test_get_data_csv(self):
        expected_data = self.site_data
        obj = ExcelParser(CSV_FILE_PATH, EXCEL_SPEC_PATH)
        result = obj.get_data()
        self.assertDictEqual(expected_data, result)

    def test_load_excel_data(self):
        result = ExcelParser.load_excel_data(EXCEL_FILE_PATH)
        self.assertIsInstance(result, Workbook)

    def test_load_csv_data(self):
        result = ExcelParser.load_excel_data(CSV_FILE_PATH)
        self.assertIsInstance(result, Workbook)

    def test_get_xl_obj_and_sheetname(self):
        result = ExcelParser.get_xl_obj_and_sheetname('Site-Information')
        self.assertEqual([None, 'Site-Information'], result)

    def test_get_xl_obj_and_sheetname_file_specified(self):
        sheet = EXCEL_FILE_PATH + ':Site-Information'
        result = ExcelParser.get_xl_obj_and_sheetname(sheet)
        self.assertIsInstance(result, list)
        self.assertIsInstance(result[0], Workbook)
        self.assertEqual(result[1], 'Site-Information')

    def test_get_xl_obj_and_sheetname_file_specified_csv(self):
        sheet = CSV_FILE_PATH + ':Site-Information'
        result = ExcelParser.get_xl_obj_and_sheetname(sheet)
        self.assertIsInstance(result, list)
        self.assertIsInstance(result[0], Workbook)
        self.assertEqual(result[1], 'Site-Information')
