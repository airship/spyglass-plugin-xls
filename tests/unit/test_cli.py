# Copyright 2019 AT&T Intellectual Property.  All other rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
from unittest import mock

from click.testing import CliRunner
from spyglass.site_processors.site_processor import SiteProcessor

from spyglass_plugin_xls.cli import generate_intermediary
from spyglass_plugin_xls.cli import generate_manifests_and_intermediary

FIXTURE_DIR = os.path.join(
    os.path.dirname(os.path.dirname(__file__)), 'shared')

EXCEL_SPEC_PATH = os.path.join(FIXTURE_DIR, 'excel_spec.yaml')

EXCEL_FILE_PATH = os.path.join(FIXTURE_DIR, 'SiteDesignSpec_v0.1.xlsx')

SITE_CONFIG_PATH = os.path.join(FIXTURE_DIR, 'site_config.yaml')

TEMPLATE_PATH = os.path.join(FIXTURE_DIR, 'templates')


@mock.patch('spyglass.cli.intermediary_processor', autospec=True)
@mock.patch('spyglass_plugin_xls.excel.ExcelPlugin', autospec=False)
def test_generate_intermediary(mock_excel_plugin, mock_intermediary_processor):
    """Test generate_intermediary (intermediary) normal execution"""
    runner = CliRunner()
    args = [
        '-x', EXCEL_FILE_PATH, '-e', EXCEL_SPEC_PATH, '-c', SITE_CONFIG_PATH,
        '-s', 'airship-seaworthy'
    ]
    result = runner.invoke(generate_intermediary, args)
    assert result.exit_code == 0
    mock_intermediary_processor.assert_called_once()
    mock_intermediary_processor.return_value.generate_intermediary_yaml\
        .assert_called_once()
    mock_intermediary_processor.return_value.dump_intermediary_file\
        .assert_called_once()


@mock.patch('spyglass.cli.intermediary_processor', autospec=True)
@mock.patch('spyglass_plugin_xls.excel.ExcelPlugin', autospec=False)
def test_generate_intermediary_intermediary_dir(
        mock_excel_plugin, mock_intermediary_processor, tmpdir):
    """Test intermediary_directory option for intermediary"""
    runner = CliRunner()
    test_dir_name = 'intermediary_test_dir'
    test_dir = tmpdir.mkdir(test_dir_name)
    args = [
        '-x', EXCEL_FILE_PATH, '-e', EXCEL_SPEC_PATH, '-c', SITE_CONFIG_PATH,
        '-s', 'airship-seaworthy', '--intermediary-dir', test_dir
    ]
    result = runner.invoke(generate_intermediary, args)
    assert result.exit_code == 0
    mock_intermediary_processor.assert_called_once()
    mock_intermediary_processor.return_value.generate_intermediary_yaml\
        .assert_called_once()
    mock_intermediary_processor.return_value.dump_intermediary_file\
        .assert_called_once_with(test_dir)


@mock.patch.object(SiteProcessor, '__init__', return_value=None)
@mock.patch('spyglass.cli.intermediary_processor', autospec=True)
@mock.patch('spyglass_plugin_xls.excel.ExcelPlugin', autospec=False)
def test_generate_manifests_and_intermediary(
        mock_excel_plugin, mock_intermediary_processor,
        mock_site_processor_init):
    """Test generate_manifests_and_intermediary (documents) normal execution"""
    runner = CliRunner()
    args = [
        '-x', EXCEL_FILE_PATH, '-e', EXCEL_SPEC_PATH, '-c', SITE_CONFIG_PATH,
        '-s', 'airship-seaworthy', '-t', TEMPLATE_PATH
    ]
    with mock.patch.object(SiteProcessor, 'render_template',
                           autospec=True) as mock_render_template:
        result = runner.invoke(generate_manifests_and_intermediary, args)
    assert result.exit_code == 0
    mock_intermediary_processor.assert_called_once()
    mock_intermediary_processor.return_value.generate_intermediary_yaml\
        .assert_called_once()
    assert not mock_intermediary_processor.return_value.dump_intermediary_file\
        .called
    mock_render_template.assert_called_once_with(mock.ANY, TEMPLATE_PATH)


@mock.patch.object(SiteProcessor, '__init__', return_value=None)
@mock.patch('spyglass.cli.intermediary_processor', autospec=True)
@mock.patch('spyglass_plugin_xls.excel.ExcelPlugin', autospec=False)
def test_generate_manifests_and_intermediary_generate_intermediary(
        mock_excel_plugin, mock_intermediary_processor,
        mock_site_processor_init):
    """Test generate_intermediary option for documents command"""
    runner = CliRunner()
    args = [
        '-x', EXCEL_FILE_PATH, '-e', EXCEL_SPEC_PATH, '-c', SITE_CONFIG_PATH,
        '-s', 'airship-seaworthy', '-t', TEMPLATE_PATH, '-i'
    ]
    with mock.patch.object(SiteProcessor, 'render_template',
                           autospec=True) as mock_render_template:
        result = runner.invoke(generate_manifests_and_intermediary, args)
    assert result.exit_code == 0
    mock_intermediary_processor.assert_called_once()
    mock_intermediary_processor.return_value.generate_intermediary_yaml\
        .assert_called_once()
    assert mock_intermediary_processor.return_value.dump_intermediary_file\
        .called
    mock_render_template.assert_called_once_with(mock.ANY, TEMPLATE_PATH)


@mock.patch.object(SiteProcessor, '__init__', return_value=None)
@mock.patch('spyglass.cli.intermediary_processor', autospec=True)
@mock.patch('spyglass_plugin_xls.excel.ExcelPlugin', autospec=False)
def test_generate_manifests_and_intermediary_intermediary_dir(
        mock_excel_plugin, mock_intermediary_processor,
        mock_site_processor_init, tmpdir):
    """Test intermediary_dir option for documents command"""
    runner = CliRunner()
    test_dir_name = 'intermediary_test_dir'
    test_dir = tmpdir.mkdir(test_dir_name)
    args = [
        '-x', EXCEL_FILE_PATH, '-e', EXCEL_SPEC_PATH, '-c', SITE_CONFIG_PATH,
        '-s', 'airship-seaworthy', '-t', TEMPLATE_PATH, '-i',
        '--intermediary-dir', test_dir
    ]
    with mock.patch.object(SiteProcessor, 'render_template',
                           autospec=True) as mock_render_template:
        result = runner.invoke(generate_manifests_and_intermediary, args)
    assert result.exit_code == 0
    mock_intermediary_processor.assert_called_once()
    mock_intermediary_processor.return_value.generate_intermediary_yaml\
        .assert_called_once()
    mock_intermediary_processor.return_value.dump_intermediary_file\
        .assert_called_once_with(test_dir)
    mock_render_template.assert_called_once_with(mock.ANY, TEMPLATE_PATH)


@mock.patch('spyglass.cli.intermediary_processor', autospec=True)
@mock.patch('spyglass_plugin_xls.excel.ExcelPlugin', autospec=False)
@mock.patch.object(SiteProcessor, '__init__', autospec=True, return_value=None)
def test_generate_manifests_and_intermediary_manifest_dir(
        mock_site_processor, mock_excel_plugin, mock_intermediary_processor,
        tmpdir):
    """Test manifest_dir option for documents command"""
    runner = CliRunner()
    test_dir_name = 'manifest_test_dir'
    test_dir = tmpdir.mkdir(test_dir_name)
    args = [
        '-x', EXCEL_FILE_PATH, '-e', EXCEL_SPEC_PATH, '-c', SITE_CONFIG_PATH,
        '-s', 'airship-seaworthy', '-t', TEMPLATE_PATH, '--manifest-dir',
        test_dir
    ]
    with mock.patch.object(SiteProcessor, 'render_template',
                           autospec=True) as mock_render_template:
        result = runner.invoke(generate_manifests_and_intermediary, args)
    assert result.exit_code == 0
    mock_intermediary_processor.assert_called_once()
    mock_intermediary_processor.return_value.generate_intermediary_yaml\
        .assert_called_once()
    assert not mock_intermediary_processor.return_value.dump_intermediary_file\
        .called
    mock_site_processor.assert_called_once_with(
        mock.ANY, mock.ANY, test_dir, False)
    mock_render_template.assert_called_once_with(mock.ANY, TEMPLATE_PATH)
